/*global alert, prompt, console*/

window.addEventListener("DOMContentLoaded", function () {
    history.replaceState(null, null, "");

    let d = document;

    let button = d.getElementById("buttonMainForm");
    let mainForm = d.getElementById("MainForm");
    let first = true;
    button.onclick = function () {
        if (mainForm.className === "row MainFormClosed m-auto") {
            mainForm.className = "row MainFormOpened m-auto";

            if (first) {
                history.pushState(null, null, "#form");
                first = false;
            } else {
                //history.go(1);
                history.pushState(null, null, "#form");
            }
        }
    };

    function checkForm() {
        const loc = "https://kiselev_oleg.gitlab.io/web8/#form";
        const loc1 = `https://kiselev_oleg.gitlab.io/web8/form/
        errorSendFrom.html`;
        if (d.location.toString() === loc1) {
            return;
        }
        if (d.location.toString() !== loc) {
            if (mainForm.className === "row MainFormOpened m-auto") {
                mainForm.className = "row MainFormClosed m-auto";
            }
            return;
        }
        if (mainForm.className === "row MainFormClosed m-auto") {
            mainForm.className = "row MainFormOpened m-auto";
        }
    }
    window.onpopstate = checkForm;
    checkForm();

    let ins = d.getElementById("MainForm").getElementsByTagName("input");
    let m = d.getElementById("MainForm").getElementsByTagName("textarea");
    let data = {
        name: "",
        email: "",
        message: "",
        agree: false
    };
    let formData = "formData";
    let t = localStorage.getItem(formData);
    if (t !== undefined && t !== null) {
        data = JSON.parse(localStorage.getItem(formData));
        data.name = String(data.name);
        data.email = String(data.email);
        data.message = String(data.message);
        data.agree = Boolean(data.agree);
    } else {
        localStorage.setItem(formData, JSON.stringify(data));
    }
    ins[0].value = data.name;
    ins[1].value = data.email;
    m[0].value = data.message;
    ins[2].checked = data.agree;

    ins[0].addEventListener("change", function () {
        data.name = ins[0].value;
        localStorage.setItem(formData, JSON.stringify(data));
    });
    ins[1].addEventListener("change", function () {
        data.email = ins[1].value;
        localStorage.setItem(formData, JSON.stringify(data));
    });
    ins[2].addEventListener("change", function () {
        data.agree = ins[2].checked;
        localStorage.setItem(formData, JSON.stringify(data));
    });
    m[0].addEventListener("change", function () {
        data.message = m[0].value;
        localStorage.setItem(formData, JSON.stringify(data));
    });

    ins[3].addEventListener("click", function () {
        let P = d.getElementById("MainForm").getElementsByTagName("p")[0];
        if (!(ins[1].value.match(/^[._-\w]+@([_-\w]+\.)+[\w]{2,3}$/))) {
            P.innerHTML = "Результат небыл отправлен<br />";
            P.innerHTML += "Возможно неверно введены данные для email";
            setTimeout(function () {
                P.innerHTML = "";
            }, 15000);
            return;
        }
        if (ins[0].value === "") {
            P.innerHTML = "Результат небыл отправлен<br />";
            P.innerHTML += "необнаружено имя";
            setTimeout(function () {
                P.innerHTML = "";
            }, 15000);
            return;
        }
        if (ins[2].checked === false) {
            P.innerHTML = "Результат небыл отправлен<br />";
            P.innerHTML += "нет согласия с обработкой данных";
            setTimeout(function () {
                P.innerHTML = "";
            }, 15000);
            return;
        }
        if (m[0].value === "") {
            P.innerHTML = "Результат небыл отправлен<br />";
            P.innerHTML += "необнаружено сообщения";
            setTimeout(function () {
                P.innerHTML = "";
            }, 15000);
            return;
        }

        data.name = "";
        data.email = "";
        data.message = "";
        data.agree = false;
        localStorage.setItem(formData, JSON.stringify(data));
        ins[0].value = data.name;
        ins[1].value = data.email;
        m[0].value = data.message;
        ins[2].checked = data.agree;

        let httpRequest = new XMLHttpRequest();
        httpRequest.open("POST", "https://formcarry.com/s/IvInFE9Z8b");
        httpRequest.setRequestHeader("Content-Type", "application/json");
        httpRequest.setRequestHeader("Accept", "application/json");
        httpRequest.send(JSON.stringify(data));
        P.innerHTML = "Результат отправлен для созранения на сервер...";
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                console.log(this.response);
                P.innerHTML += "<br /> сервер получил результат";
                setTimeout(function () {
                    const loc = "https://kiselev_oleg.gitlab.io/web8/";
                    history.pushState(null, null, loc);
                    mainForm.className = "row MainFormClosed m-auto";
                }, 3000);
            }
        };
        setTimeout(function () {
            P.innerHTML = "";
        }, 35000);
        return;
    });
});

function addThing() {
    if (document.getElementById("name").innerHTML === "!Неизвестно имя:") {
        return;
    }

    let name = document.getElementsByName("name")[0].value;
    let cost = document.getElementsByName("cost")[0].value;
    let number = document.getElementsByName("number")[0].value;

    if (null === name.match(/^[\w,а-яб,А-Я]{1,}$/)) {
        alert("неправильно указано имя");
        return;
    }
    if (null === cost.match(/^\d{1,}(.\d{1,}){0,1}$/)) {
        alert("неправильно указана цена");
        return;
    }
    if (null === number.match(/^\d{1,}$/)) {
        alert("неправильно указано количество");
        return;
    }

    document.getElementById("result").insertAdjacentHTML("beforeBegin", `
        <tr>
            <td>` + name + `</td>
            <td>` + cost + `</td>
            <td>` + number + `</td>
            <td>` + parseFloat(cost) * parseInt(number) + `</td>
        </tr>`);

    document.getElementById("maincost").innerHTML = String(
        parseFloat(document.getElementById("maincost").innerHTML) +
        parseFloat(cost) * parseInt(number)
    );
}
function hide() {
    let b = document.getElementById("hide");
    let tr = document.getElementById("table").getElementsByTagName("tr");
    let i;
    if (b.value === "скрыть") {
        b.value = "открыть";

        for (i = 1; i < tr.length - 1; i += 1) {
            tr[i].style.display = "none";
        }
    } else {
        b.value = "скрыть";

        for (i = 1; i < tr.length - 1; i += 1) {
            tr[i].style.display = "table-row";
        }
    }
}
function name() {
    if (document.getElementById("name").innerHTML !== "!Неизвестно имя:") {
        return;
    }
    let n = prompt("Для использования калькулятора введите имя ");
    if (n === null) {
        return;
    }
    if (null === n.match(/^[\w,а-яб,А-Я]{1,}$/)) {
        alert("ввведено некоректное имя");
        return;
    }
    document.getElementById("name").innerHTML = String(n + ":");
    document.getElementById("name").className = "dg-ligth";
}
window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("add").addEventListener("click", name);
    document.getElementById("add").addEventListener("click", addThing);
    document.getElementById("hide").addEventListener("click", hide);
});

function selectCostTable() {
    /*return [
        { name: "product1", value: 80 },

        { name: "product2", value: 250 },
        { name: "product2_checkbox1", value: 80 },
        { name: "product2_checkbox2", value: 90 },
        { name: "product2_checkbox3", value: 50 },

        { name: "product3", value: 150 },
        { name: "product3_radio1", value: 1.5 },
        { name: "product3_radio2", value: 1.3 },
        { name: "product3_radio3", value: 2.2 },
        { name: "product3_radio4", value: 2.3 },
        { name: "product3_radio5", value: 1.9 }
    ];*/
    let d = document;
    let s = "select";
    //let s0 = "select0";
    let s1 = "select1";
    let s2 = "select2";
    return [
        {
            name: d.getElementById(s).getElementsByTagName("option")[0],
            value: 80
        },

        {
            name: d.getElementById(s).getElementsByTagName("option")[1],
            value: 250
        },
        {
            name: d.getElementById(s1).getElementsByTagName("input")[0],
            value: 80
        },
        {
            name: d.getElementById(s1).getElementsByTagName("input")[1],
            value: 90
        },
        {
            name: d.getElementById(s1).getElementsByTagName("input")[2],
            value: 50
        },

        {
            name: d.getElementById(s).getElementsByTagName("option")[2],
            value: 150
        },
        {
            name: d.getElementById(s2).getElementsByTagName("input")[0],
            value: 1.5
        },
        {
            name: d.getElementById(s2).getElementsByTagName("input")[1],
            value: 1.3
        },
        {
            name: d.getElementById(s2).getElementsByTagName("input")[2],
            value: 2.2
        },
        {
            name: d.getElementById(s2).getElementsByTagName("input")[3],
            value: 2.3
        },
        {
            name: d.getElementById(s2).getElementsByTagName("input")[4],
            value: 1.9
        }
    ];
}
function selectCost(o) {
    let sel = selectCostTable();
    let i;
    for (i = 0; i < sel.length; i += 1) {
        if (sel[i].name === o) {
            return sel[i].value;
        }
    }

    return null;
}
function selectCountCost() {
    let d = document;

    if (null === d.getElementById("selectNumber").value.match(/^\d{1,}$/)) {
        d.getElementById("selectCost").innerHTML = "!неправильноe количество!";
        return;
    }

    let s = d.getElementById("select");
    let res = selectCost(s.getElementsByTagName("option")[s.value]);
    let si = d.getElementById("select" + s.value);
    si = si.getElementsByTagName("input");
    let i;
    for (i = 0; i < si.length; i += 1) {
        if (si[i].checked && parseInt(s.value) !== 2) {
            res += selectCost(si[i]);
        } else if (si[i].checked) {
            res *= selectCost(si[i]);
        }
    }
    res *= parseInt(d.getElementById("selectNumber").value);

    d.getElementById("selectCost").innerHTML = res;
}
function selectAdd() {
    if (document.getElementById("name").innerHTML === "!Неизвестно имя:") {
        return;
    }

    let number = document.getElementById("selectNumber").value;
    if (null === number.match(/^\d{1,}$/)) {
        alert("неправильно указано количество в калькуляторе товаров");
        return;
    }
    let n = document.getElementById("select");
    n = n.getElementsByTagName("option");
    n = n[document.getElementById("select").value].innerHTML;
    let cost = document.getElementById("selectCost").innerHTML;

    document.getElementById("result").insertAdjacentHTML("beforeBegin", `
        <tr>
            <td>` + n + `</td>
            <td>` + parseInt(cost) / parseInt(number) + `</td>
            <td>` + number + `</td>
            <td>` + parseFloat(cost) + `</td>
        </tr>`);

    document.getElementById("maincost").innerHTML = String(
        parseFloat(document.getElementById("maincost").innerHTML) +
        parseFloat(cost)
    );
}
window.addEventListener("DOMContentLoaded", function () {
    let d = document;
    d.getElementById("select").addEventListener("change", selectCountCost);
    function clearSelecti() {
        let t = d.getElementById("select");
        let i;
        for (i = 0; i < t.getElementsByTagName("option").length; i += 1) {
            d.getElementById("select" + i).style.display = "none";
        }
        d.getElementById("select" + t.value).style.display = "inline-block";
    }
    d.getElementById("select").addEventListener("change", clearSelecti);
    clearSelecti();

    let i;
    let j;
    for (i = 0; i < 3; i += 1) {
        let s = d.getElementById("select" + i).getElementsByTagName("input");
        for (j = 0; j < s.length; j += 1) {
            s[j].addEventListener("change", selectCountCost);
        }
    }

    let scc = selectCountCost;
    d.getElementById("selectAdd").addEventListener("click", name);
    d.getElementById("selectAdd").addEventListener("click", scc);
    d.getElementById("selectAdd").addEventListener("click", selectAdd);
    d.getElementById("selectNumber").addEventListener("change", scc);
    scc();
});
